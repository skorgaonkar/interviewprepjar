package sanket.free.distribution.algorithms.sorting;

public class ArraySorter {
	
	public static void mSort(int[] input){
		mergeSort(input, 0, input.length-1);
	}

	public static void mergeSort(int[] input, int start, int end){
		if( start < end ){
			int mid = (int)( (start+end)/2.0d );
			mergeSort(input, start, mid);
			mergeSort(input, mid+1, end);
			merge(input, start, mid, end);
		}
	}
	
	public static void merge(int[] input, int start, int mid, int end){
		int tIndex = 0;
		int left = start;
		int right = mid+1;
		int[] temp = new int[end-start+1];
		
		while( (left<=mid) && (right<=end) ){
			if( input[left] < input[right] ){
				temp[tIndex] = input[left];
				left++;
				tIndex++;
			} else {
				temp[tIndex] = input[right];
				tIndex++;
				right++;
			}
		}
		
		//copy the left over elements
		while( left <= mid ){
			temp[tIndex] = input[left];
			left++; tIndex++;
		}

		while( right <= end ){
			temp[tIndex] = input[right];
			right++;
			tIndex++;
		}
		
		//copy temp into input
		for(int i=start; i<=end; i++){
			input[i] = temp[i-start];
		}
	}
	
}
