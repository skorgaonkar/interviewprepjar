package sanket.free.distribution.dataStructures.LinkedLists;

public class LinkedList {
	private LNode head;

	public LinkedList() {
		head = null;
	}

	public void insert(int val) {
		if (head == null) {
			head = new LNode(val);
			return;
		}

		for (LNode current = head; current != null; current = current.getNext()) {
			if (current.getNext() == null) {
				LNode temp = new LNode(val);
				current.setNext(temp);
				break;
			}
		}
	}

	public boolean search(int val) {
		LNode temp = head;
		while (temp != null) {
			if (temp.getValue() == val) {
				return true;
			}
			temp = temp.getNext();
		}

		return false;
	}

	public LNode getHead() {
		return head;
	}

	public void setHead(LNode head) {
		this.head = head;
	}

	public int count() {
		int count = 0;
		for (LNode current = head; current != null; current = current.getNext()) {
			count++;
		}
		return count;
	}

	public void delete(int val) {
		if (head == null) {
			System.out.println("List is empty!");
		} else if (head.getValue() == val) {
			head = head.getNext();
			System.out.println("Value " + val + " has been deleted");
		} else {
			LNode temp = new LNode();
			temp.setNext(head);
			delete(temp, val);
		}
	}

	private void delete(LNode current, int val) {
		if (current.getNext() == null) {
			System.out.println("Sorry value " + val + " not found!");
			return;
		}

		if (current.getNext().getValue() == val) {
			current.setNext(current.getNext().getNext());
			System.out.println("Value " + val + " has been deleted");
		} else {
			delete(current.getNext(), val);
		}
	}

	public void printList() {
		if (head == null) {
			System.out.println("List is empty!");
		}

		LNode temp = head;
		while (temp != null) {
			System.out.println(temp.getValue());
			temp = temp.getNext();
		}
	}

}
