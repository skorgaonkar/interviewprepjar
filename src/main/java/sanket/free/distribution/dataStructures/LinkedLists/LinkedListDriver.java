package sanket.free.distribution.dataStructures.LinkedLists;

public class LinkedListDriver {
	public static LinkedList list = new LinkedList();

	public static void mainMyLinkedListDriver(String[] args) {
		int[] input = { 10, 20, 30, 40 };
		insertArray(input);
		list.delete(30);
		list.printList();
	}

	private static void insertArray(int[] A) {
		for (int i : A) {
			list.insert(i);
		}
	}

	private static void deleteArray(int[] A) {
		for (int i : A) {
			list.delete(i);
		}
	}

	public static void main(String[] args) {
		java.util.LinkedList list = new java.util.LinkedList();
		int[] input = { 20, 30, 20, 30, 50, 50, 59 };
		for (int i : input) {
			list.add(i);
		}

		list.clear();
		System.out.println(list.size());

	}

}
