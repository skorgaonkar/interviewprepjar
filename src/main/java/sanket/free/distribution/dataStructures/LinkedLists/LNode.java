package sanket.free.distribution.dataStructures.LinkedLists;

public class LNode {
	private int value;
	private LNode next;

	LNode() {
		value = 0;
		next = null;
	}

	LNode(int i) {
		value = i;
		next = null;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public LNode getNext() {
		return next;
	}

	public void setNext(LNode next) {
		this.next = next;
	}

}
