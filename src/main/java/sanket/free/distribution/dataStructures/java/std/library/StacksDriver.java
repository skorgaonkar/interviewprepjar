package sanket.free.distribution.dataStructures.java.std.library;

import java.util.Stack;

public class StacksDriver {
	public static Stack stack = new Stack();

	public static void main(String[] args) {
		int[] input = { 23, 34, 5, 56, 22, 4 };
		for (int i : input) {
			stack.push(i);
		}

		while (!stack.isEmpty()) {
			System.out.println(stack.pop());
		}
	}
}
