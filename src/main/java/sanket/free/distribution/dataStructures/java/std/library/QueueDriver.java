package sanket.free.distribution.dataStructures.java.std.library;

import java.util.PriorityQueue;

public class QueueDriver {
	public static  PriorityQueue<Integer> queue = new PriorityQueue<Integer>();
	
	public static void main(String[] args){
		int[] input = { 23, 45,12, 45, 34, 2};
		
		for(int i:input){
			queue.add(i);
		}
		
		System.out.println(queue.toString());
	}
	
	
}
