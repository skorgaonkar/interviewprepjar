package sanket.free.distribution.dataStructures.BinarySearchTree;

import java.util.ArrayList;

public class BSTDriver {
	static BinarySearchTree tree = new BinarySearchTree();

	private static void insertNums(BinarySearchTree tree, int[] A) {
		for (int i : A) {
			tree.insertValue(i);
		}
	}

	private static void deleteNums(BinarySearchTree tree, int[] A) {
		for (int i : A) {
			tree.delete(i);
			tree.printTree();
		}
	}

	public static void mainBST(String[] args) {
		int[] input = { 30, 20, 40, 15, 25, 13, 18, 23, 27 };
		// int[] input = {30,20};
		insertNums(tree, input);
		tree.printTree();
		deleteNums(tree, input);
		tree.printTree();

		tree.insertValue(20);
		tree.printTree();
		tree.delete(10);
		tree.printTree();
		tree.delete(20);
		tree.printTree();
	}

	public static void main(String[] args) {
		/*
		 * Create a binary tree
		 */
		BNode root = new BNode(10);
		root.setLeft(new BNode(20));
		root.setRight(new BNode(20));
		root.getLeft().setLeft(new BNode(30));
		;
		root.getLeft().setRight(new BNode(35));
		root.getRight().setLeft(new BNode(35));
		root.getRight().setRight(new BNode(30));

		printBinaryTree(root);

	}

	private static void printBinaryTree(BNode root) {
		if (root == null) {
			return;
		}

		printBinaryTree(root.getLeft());
		System.out.println(root.getValue());
		printBinaryTree(root.getRight());
	}
}
