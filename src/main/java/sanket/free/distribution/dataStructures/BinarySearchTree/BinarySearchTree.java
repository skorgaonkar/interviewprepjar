/**
 * This class provides the functionality of the Binary Search Tree.
 * 
 *  It will provide the following API:
 *  	1) insert(int)
 *  	2) insert(BNode)
 *  	3) delete(int value)
 *  	4) search(int value) - returns BNode or null
 *  	5) count(BNode x) - returns number of nodes in the BST including x
 */

package sanket.free.distribution.dataStructures.BinarySearchTree;

public class BinarySearchTree {
	public BNode root;

	public BinarySearchTree() {
		root = null;
	}

	public void insertValue(int val) {
		this.root = insertValue(this.root, val);
	}

	public void printTree() {
		if (root == null) {
			System.out.println("Tree is empty!");
		} else {
			printTree(root);
		}
	}

	public void delete(int val) {
		if (root == null) {
			System.out.println("Tree is empty!");
			return;
		}

		this.root = deleteVal(this.root, val);
	}

	/**
	 * @param x
	 *            - The node from which to count the population.
	 * @return - returns the number of nodes in the sub-tree starting with x
	 *         including the node x.
	 */
	public int countNodes(BNode x) {
		if (x == null) {
			return 0;
		} else {
			return 1 + (countNodes(x.getLeft()) + countNodes(x.getRight()));
		}
	}

	public BNode searchValue(int value) {
		if (root == null) {
			return null;
		} else if (root.getValue() == value) {
			return root;
		} else if (root.getValue() > value) {
			return searchValue(root.getLeft(), value);
		} else {
			return searchValue(root.getRight(), value);
		}
	}

	private BNode deleteVal(BNode root, int val) {
		if (root == null) {
			System.out.println("Number " + val + " was not found!");
			return null;
		}

		if (root.getValue() > val) {
			root.setLeft(deleteVal(root.getLeft(), val));
			return root;
		} else if (root.getValue() < val) {
			root.setRight(deleteVal(root.getRight(), val));
			return root;
		} else {
			System.out.println("Number " + val + " has been deleted!");
			// Number is present and needs to be deleted
			if (isLeafNode(root)) {
				root = null;
				return null;
			} else if (hasOnlyRightChild(root)) {
				root.setValue(root.getRight().getValue());
				root.setRight(null);
				return root;
			} else if (hasOnlyLeftChild(root)) {
				root.setValue(root.getLeft().getValue());
				root.setLeft(null);
				return root;
			} else {
				// value is present and has 2 children
				int replacementVal = getRightmostNode(root.getLeft()).getValue();
				root.setValue(replacementVal);

				BNode temp = new BNode();
				temp.setRight(root.getLeft());
				setRightmostNull(temp);
				return root;
			}
		}
	}

	private void deleteRightmostNode(BNode root) {
		if (root.getRight() == null) {
			root = null;
		} else {
			deleteRightmostNode(root.getRight());
		}
	}

	private BNode setRightmostNull(BNode x) {
		if (x.getRight() == null) {
			return null;
		} else {
			x.setRight(setRightmostNull(x.getRight()));
			return x;
		}

	}

	private BNode searchValue(BNode root, int value) {
		if (root == null) {
			return null;
		} else if (root.getValue() == value) {
			return root;
		} else if (root.getValue() > value) {
			return searchValue(root.getLeft(), value);
		} else {
			return searchValue(root.getRight(), value);
		}
	}

	private BNode insertValue(BNode x, int val) {
		// null, create new node and return
		if (x == null) {
			x = new BNode(val);
		} else if (x.getValue() > val) {
			x.setLeft(insertValue(x.left, val));
		} else if (x.getValue() < val) {
			x.setRight(insertValue(x.right, val));
		}
		return x;
	}

	private void printTree(BNode x) {
		if (x == null) {
			// nothing to print here
		} else {
			System.out.println(x.getValue());
			if (x.getLeft() != null) {
				printTree(x.getLeft());
			}
			if (x.getRight() != null) {
				printTree(x.getRight());
			}
		}
	}

	private BNode getLeftmostNode(BNode x) {
		if (x.getLeft() == null) {
			return x;
		} else {
			return getLeftmostNode(x.getLeft());
		}
	}

	private BNode getRightmostNode(BNode x) {
		if (x.getRight() == null) {
			return x;
		} else {
			return getRightmostNode(x.getRight());
		}
	}

	private boolean isLeafNode(BNode x) {
		return (x.getLeft() == null && x.getRight() == null);
	}

	private boolean hasOnlyRightChild(BNode x) {
		return (x.getLeft() == null && x.getRight() != null);
	}

	private boolean hasOnlyLeftChild(BNode x) {
		return (x.getLeft() != null && x.getRight() == null);
	}

	private boolean hasTwoChildren(BNode x) {
		return (x.getRight() != null && x.getLeft() != null);
	}
}
