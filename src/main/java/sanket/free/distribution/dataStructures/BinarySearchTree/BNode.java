package sanket.free.distribution.dataStructures.BinarySearchTree;

public class BNode {
	int value;
	BNode left;
	BNode right;

	BNode(int value) {
		this.value = value;
		this.left = null;
		this.right = null;
	}

	BNode() {
		this.right = null;
		this.left = null;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public BNode getLeft() {
		return left;
	}

	public void setLeft(BNode left) {
		this.left = left;
	}

	public BNode getRight() {
		return right;
	}

	public void setRight(BNode right) {
		this.right = right;
	}

	public boolean equalValue(BNode test) {
		return (this.value == test.getValue());
	}

	public boolean isLeaf() {
		return (this.getRight() == null && this.getLeft() == null);
	}

}
