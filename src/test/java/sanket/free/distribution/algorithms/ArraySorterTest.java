package sanket.free.distribution.algorithms;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import sanket.free.distribution.algorithms.sorting.ArraySorter;

public class ArraySorterTest extends TestCase {

	private int[] unsorted = { 12, 2, 3, 1, 0, -10, 34, 64, 45 };
	private int[] sorted = { -10, 0, 1, 2, 3, 12, 34, 45, 64 };
	
	private static boolean arrayEqual(int[] A, int[] B) {
		boolean equal = true;

		if (A.length != B.length) {
			return !equal;
		}

		for (int i = 0; i < A.length; i++) {
			if (A[i] != B[i]) {
				equal = false;
				break;
			}
		}
		return equal;
	}

	@Before
	public void setUp() {
		
	}

	@Test
	public void test() {
		ArraySorter.mSort(unsorted);
		assertTrue( arrayEqual(unsorted, sorted));
	}

}
