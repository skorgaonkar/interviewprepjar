package sanket.free.distribution.dataStructures;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sanket.free.distribution.dataStructures.LinkedLists.LinkedList;

public class LinkedListTest {

	LinkedList list = new LinkedList();
	int[] input = { 20, 30, 20, 30, 50, 50, 59 };

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		for (int i : input) {
			list.insert(i);
		}
	}

	@After
	public void tearDown() throws Exception {
		list.printList();
		list = null;
	}

	@Test
	public void test() {
		assertTrue(list.count() == 7);
		assertFalse(list.search(23));
		assertTrue(list.search(20));

		while (list.search(20)) {
			list.delete(20);
		}

		assertFalse(list.search(20));

		list.insert(27);
		for (int i = 0; i < input.length; i++) {
			list.delete(input[i]);
		}

		assertTrue(list.count() == 1);
		assertTrue(list.search(27));
		list.delete(27);
		assertFalse(list.search(27));
		assertTrue(list.getHead() == null);
		assertTrue(list.count() == 0);
	}

}
