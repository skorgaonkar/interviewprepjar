package sanket.free.distribution.dataStructures;

import org.junit.Test;

import junit.framework.TestCase;
import sanket.free.distribution.dataStructures.BinarySearchTree.BinarySearchTree;

public class BinarySearchTreeTest extends TestCase {

	BinarySearchTree tree;

	protected void setUp() throws Exception {
		super.setUp();
		tree = new BinarySearchTree();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		tree = null;
	}

	@Test
	public void testInsertRow() {
		tree.insertValue(20);
		tree.insertValue(30);
		tree.insertValue(15);
		tree.insertValue(45);
		tree.insertValue(30);

		assertTrue(tree.root.getValue() == 20);
		assertTrue(tree.root.getRight().getValue() == 30);
		assertTrue(tree.root.getRight().getRight().getValue() == 45);
		assertTrue(tree.root.getLeft().getValue() == 15);
	}

	@Test
	public void testCountNodes() {
		tree.insertValue(20);
		tree.insertValue(30);
		tree.insertValue(15);
		tree.insertValue(45);
		tree.insertValue(30);
		assertTrue(tree.countNodes(tree.root) == 4);
		assertTrue(tree.countNodes(tree.root.getRight()) == 2);
	}

}
